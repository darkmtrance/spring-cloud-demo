package com.matomaylla.hotels.controller;

import java.util.List;

import com.matomaylla.hotels.config.HotelsServiceConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.matomaylla.hotels.model.Hotel;
import com.matomaylla.hotels.model.HotelRooms;
import com.matomaylla.hotels.model.PropertiesHotels;
import com.matomaylla.hotels.services.IHotelService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;

@RestController
public class HotelController {
	
	private static final Logger logger = LoggerFactory.getLogger(HotelController.class);
	
	@Autowired
	private IHotelService service;
	
	@Autowired
	private HotelsServiceConfiguration configHotels;
	
	@GetMapping("hotels")
	public List<Hotel> search(){
		logger.info("inicio de método search()");
		return (List<Hotel>) this.service.search();	
	}
	
	@GetMapping("hotels/{hotelId}")
	//@CircuitBreaker(name="searchHotelByIdSupportCB", fallbackMethod = "searchHotelByIdAltervative")
	//@Retry(name="searchHotelByIdSupportRetry", fallbackMethod = "searchHotelByIdAltervative")
	@RateLimiter(name="searchHotelByIdSupportRateLimiter", fallbackMethod = "searchHotelByIdAltervative")
	public HotelRooms searchHotelById(@PathVariable long hotelId){
		logger.info("inicio de método searchHotelById()");
		return this.service.searchHotelById(hotelId);
	}
	
	public HotelRooms searchHotelByIdAltervative(@PathVariable long hotelId, Throwable thr){
		return this.service.searchHotelByIdwithoutRooms(hotelId);
	}
	
	@GetMapping("/hotels/read/properties")
	public String getPropertiesHotels() throws JsonProcessingException {
		ObjectWriter owj = new ObjectMapper().writer().withDefaultPrettyPrinter();
		PropertiesHotels propHotels = new PropertiesHotels(configHotels.getMsg(), configHotels.getBuildVersion(),
				configHotels.getMailDetails()); 
		String jsonString = owj.writeValueAsString(propHotels);
		return jsonString;
	}

}
