package com.matomaylla.hotels.services;

import java.util.List;

import com.matomaylla.hotels.model.Hotel;
import com.matomaylla.hotels.model.HotelRooms;

public interface IHotelService {
	
	List<Hotel> search();
	HotelRooms searchHotelById(long hotelId);
	HotelRooms searchHotelByIdwithoutRooms(long hotelId);
	

}
