package com.matomaylla.hotels.dao;

import org.springframework.data.repository.CrudRepository;

import com.matomaylla.hotels.model.Hotel;

public interface IHotelDao extends CrudRepository<Hotel, Long> {

}
