package com.matomaylla.rooms.controller;

import java.util.List;

import com.matomaylla.rooms.config.RoomsServiceConfiguration;
import com.matomaylla.rooms.model.PropertiesRooms;
import com.matomaylla.rooms.model.Room;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.matomaylla.rooms.services.IRoomService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@RestController
public class RoomController {
	
	private static final Logger logger = LoggerFactory.getLogger(RoomController.class);

	@Autowired
	private IRoomService service;
	
	@Autowired
	private RoomsServiceConfiguration configRooms;
	
	
	@GetMapping("rooms")
	public List<Room> search(){
		logger.info("inicio de método search()");
		return (List<Room>) this.service.search();	
	}
	
	@GetMapping("rooms/{id}")
	public List<Room> searchByHotelId(@PathVariable long id){
		logger.info("inicio de método searchByHotelId()");
		return (List<Room>) this.service.searchRoomByHotelId(id);	
	}
	
	@GetMapping("/rooms/read/properties")
	public String getPropertiesHotels() throws JsonProcessingException {
		ObjectWriter owj = new ObjectMapper().writer().withDefaultPrettyPrinter();
		PropertiesRooms propRooms = new PropertiesRooms( configRooms.getMsg(), configRooms.getBuildVersion(),
				configRooms.getMailDetails()); 
		String jsonString = owj.writeValueAsString(propRooms);
		return jsonString;
	}
	
	
}
