package com.matomaylla.rooms.dao;

import java.util.List;

import com.matomaylla.rooms.model.Room;
import org.springframework.data.repository.CrudRepository;

public interface IRoomDao extends CrudRepository<Room, Long>{
	
	public List<Room> findByHotelId(long hotelId);

}
