package com.matomaylla.rooms.services;

import java.util.List;

import com.matomaylla.rooms.dao.IRoomDao;
import com.matomaylla.rooms.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomServiceImpl implements IRoomService {
	
	@Autowired
	private IRoomDao roomDao;

	@Override
	public List<Room> search() {
		return (List<Room>) roomDao.findAll();
	}

	@Override
	public List<Room> searchRoomByHotelId(long hotelId) {
		 List<Room> rooms = this.roomDao.findByHotelId(hotelId);
		 return rooms;
	}

}
