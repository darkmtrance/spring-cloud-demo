package com.matomaylla.rooms.services;

import java.util.List;

import com.matomaylla.rooms.model.Room;

public interface IRoomService {
	
	List<Room> search();
	List<Room> searchRoomByHotelId(long hotelId);

}
