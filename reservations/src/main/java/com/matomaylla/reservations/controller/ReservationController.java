package com.matomaylla.reservations.controller;

import java.util.List;

import com.matomaylla.reservations.config.ReservationServiceConfiguration;
import com.matomaylla.reservations.model.PropertiesReservations;
import com.matomaylla.reservations.model.Reservation;
import com.matomaylla.reservations.services.IReservationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@RestController
public class ReservationController {
	
	private static final Logger logger = LoggerFactory.getLogger(ReservationController.class);

	
	@Autowired
	private IReservationService service;
	
	@Autowired
	private ReservationServiceConfiguration configReservations;
	

	@GetMapping("reservations")
	public List<Reservation> search(){
		logger.info("inicio de método search()");
		return (List<Reservation>) this.service.search();	
	}
	
	@GetMapping("/reservations/read/properties")
	public String getPropertiesHotels() throws JsonProcessingException {
		ObjectWriter owj = new ObjectMapper().writer().withDefaultPrettyPrinter();
		PropertiesReservations propReservations = new PropertiesReservations( configReservations.getMsg(), configReservations.getBuildVersion(),
				configReservations.getMailDetails()); 
		String jsonString = owj.writeValueAsString(propReservations);
		return jsonString;
	}

}
