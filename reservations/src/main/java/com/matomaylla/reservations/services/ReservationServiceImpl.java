package com.matomaylla.reservations.services;

import java.util.List;

import com.matomaylla.reservations.dao.IReservationDao;
import com.matomaylla.reservations.model.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationServiceImpl implements IReservationService {

	@Autowired
	private IReservationDao reservationDao;
	
	@Override
	public List<Reservation> search() {
		return (List<Reservation>) reservationDao.findAll();
	}

}
