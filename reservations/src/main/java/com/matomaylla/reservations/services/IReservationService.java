package com.matomaylla.reservations.services;

import java.util.List;

import com.matomaylla.reservations.model.Reservation;


public interface IReservationService {
	
	List<Reservation> search();

}
