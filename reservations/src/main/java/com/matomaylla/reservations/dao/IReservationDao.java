package com.matomaylla.reservations.dao;

import com.matomaylla.reservations.model.Reservation;
import org.springframework.data.repository.CrudRepository;

public interface IReservationDao extends CrudRepository<Reservation, Long>{

}
